import java.util.Scanner;

public class Parameter {
    public static void BMI(double weight, int height) {
        System.out.println("BMI kamu : " + Math.pow(weight / height, 2));
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Berat Kamu : ");
        double berat = input.nextDouble();
        System.out.println("Masukkan Tinggi Kamu : ");
        int tinggi = input.nextInt();

        BMI(berat, tinggi);
        //BMI(80, 180);
    }
}
