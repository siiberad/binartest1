public class nestedLoop1 {
    public static void main(String[]args) {
        for (int row = 1; row <= 5; row++) {
            System.out.print("row " + row + ": ");
            for (int col = 0; col <= 10; col++){
                System.out.print(col + " ");
            }
            System.out.println();
        }
    }
}

