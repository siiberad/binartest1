import java.util.Scanner;

public class ifElse2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Pemain Pertama Pilih Batu, Kertas, Gunting");
        String pertama = input.nextLine();
        System.out.println("Pemain Kedua Pilih Batu, Kertas, Gunting");
        String kedua = input.nextLine();

        if ((pertama.equalsIgnoreCase("batu") && kedua.equalsIgnoreCase("gunting")) || (pertama.equalsIgnoreCase("kertas") && kedua.equalsIgnoreCase("batu")) || (pertama.equalsIgnoreCase("gunting") && kedua.equalsIgnoreCase("kertas"))) {
                System.out.println("pemain pertama menang");
        } else if ((pertama.equalsIgnoreCase("batu") && kedua.equalsIgnoreCase("kertas")) || (pertama.equalsIgnoreCase("kertas") && kedua.equalsIgnoreCase("gunting")) || (pertama.equalsIgnoreCase("gunting") && kedua.equalsIgnoreCase("batu"))){
                System.out.println("pemain pertama kalah");
        } else if ((pertama.equalsIgnoreCase("batu") && kedua.equalsIgnoreCase("batu")) || (pertama.equalsIgnoreCase("kertas") && kedua.equalsIgnoreCase("kertas")) || (pertama.equalsIgnoreCase("gunting") && kedua.equalsIgnoreCase("gunting"))) {
                System.out.println("seri");
        }
    }
}
