import java.util.Scanner;

public class CC2 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Kata : ");
        String pa = input.nextLine();
        System.out.println("-----------------");
        if (palin(pa))
            System.out.println(pa +" == Palindrome");
        else
            System.out.println(pa +" != Palindrome");
    }


    public static boolean palin(String c) {
        int left = 0;
        int right = c.length() - 1;

        while (left < right) {
            if (c.charAt(left) != c.charAt(right))
                return false;
            left++;
            right--;
            }
            return true;

        }
}